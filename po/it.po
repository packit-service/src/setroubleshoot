# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# antoniomontag <antonio.montagnani@alice.it>, 2013
# Dimitris Glezos <glezos@transifex.com>, 2011
# Francesco Tombolini <tombo@adamantio.net>, 2006, 2007, 2008, 2009, 2011
# fvalen <fvalen@redhat.com>, 2013
# Gianluca Busiello <busiello@ceinge.unina.it>, 2008
# Luca Manlio De Lisi <lukefiltroman@gmail.com>, 2011
# Luca Manlio De Lisi <lukefiltroman@gmail.com>, 2011
# Mario Santagiuliana <fedora at marionline.it>, 2012
# massimo81 <mhacknetxp@hotmail.com>, 2013
# Silvio Pierro <perplesso82@gmail.com>, 2012
# Silvio Pierro <perplesso82@gmail.com>, 2008-2009,2013
# Vit Mojzis <vmojzis@redhat.com>, 2017. #zanata
# Ludek Janda <ljanda@redhat.com>, 2018. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-30 17:47+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2018-11-20 03:11-0500\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Italian (http://www.transifex.com/projects/p/fedora/language/"
"it/)\n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

#: ../setroubleshoot.desktop.in.h:1
msgid "SELinux Troubleshooter"
msgstr "Risoluzione problemi SELinux"

#: ../setroubleshoot.desktop.in.h:2
msgid "Troubleshoot SELinux access denials"
msgstr "Risoluzione problemi SELinux accessi rifiutati"

#: ../setroubleshoot.desktop.in.h:3
msgid "policy;security;selinux;avc;permission;mac;alert;sealert;"
msgstr "policy;security;selinux;avc;permission;mac;alert;sealert;"

#: ../src/setroubleshoot/audit_data.py:923
#, python-format
msgid "port %s"
msgstr "porte %s"

#: ../src/setroubleshoot/audit_data.py:925 ../src/setroubleshoot/browser.py:779
#: ../src/setroubleshoot/browser.py:854 ../src/setroubleshoot/rpc.py:558
#: ../src/setroubleshoot/util.py:306
msgid "Unknown"
msgstr "Sconosciuto"

#: ../src/setroubleshoot/audit_data.py:1007
#, python-format
msgid "%s \n"
"**** Recorded AVC is allowed in current policy ****\n"
msgstr ""

#: ../src/setroubleshoot/audit_data.py:1009
#, python-format
msgid ""
"%s \n"
"**** Recorded AVC is dontaudited in current policy. 'semodule -B' will turn "
"on dontaudit rules ****\n"
msgstr ""

#: ../src/setroubleshoot/audit_data.py:1011
msgid "Must call policy_init first"
msgstr "Evocare prima policy_init"

#: ../src/setroubleshoot/audit_data.py:1013
#, python-format
msgid "%s \n"
"**** Invalid AVC: bad target context ****\n"
msgstr ""

#: ../src/setroubleshoot/audit_data.py:1015
#, python-format
msgid "%s \n"
"**** Invalid AVC: bad source context ****\n"
msgstr ""

#: ../src/setroubleshoot/audit_data.py:1017
#, python-format
msgid "%s \n"
"**** Invalid AVC: bad type class ****\n"
msgstr ""

#: ../src/setroubleshoot/audit_data.py:1019
#, python-format
msgid "%s \n"
"**** Invalid AVC: bad permission ****\n"
msgstr ""

#: ../src/setroubleshoot/audit_data.py:1021
msgid "Error during access vector computation"
msgstr "Errore durante il calcolo del vettore di accesso"

#: ../src/setroubleshoot/browser.py:208 ../gui/browser.glade.h:14
msgid "SELinux Alert Browser"
msgstr "Browser di allarmi di SELinux"

#: ../src/setroubleshoot/browser.py:212 ../gui/browser.glade.h:21
msgid "The source process:"
msgstr "Il processo sorgente:"

#: ../src/setroubleshoot/browser.py:217 ../gui/browser.glade.h:17
msgid "Yes"
msgstr "Si"

#: ../src/setroubleshoot/browser.py:219 ../gui/browser.glade.h:19
msgid "No"
msgstr "No"

#: ../src/setroubleshoot/browser.py:224 ../gui/browser.glade.h:22
msgid "Attempted this access:"
msgstr "Tentato questo accesso:"

#: ../src/setroubleshoot/browser.py:238 ../gui/browser.glade.h:12
msgid "SETroubleshoot Details Window"
msgstr "Finestra dei dettagli della risoluzione di SETroubleshoot"

#: ../src/setroubleshoot/browser.py:241 ../gui/browser.glade.h:16
msgid "Would you like to receive alerts?"
msgstr "Si vogliono ricevere gli allarmi?"

#: ../src/setroubleshoot/browser.py:245 ../gui/browser.glade.h:26
msgid "Notify Admin"
msgstr "Notifica all'amministratore"

#: ../src/setroubleshoot/browser.py:248 ../src/setroubleshoot/browser.py:254
#: ../gui/browser.glade.h:8
msgid "Troubleshoot"
msgstr "Risoluzione dei problemi"

#: ../src/setroubleshoot/browser.py:251 ../gui/browser.glade.h:28
msgid "Details"
msgstr "Dettagli"

#: ../src/setroubleshoot/browser.py:258 ../gui/browser.glade.h:7
msgid "SETroubleshoot Alert List"
msgstr "Lista avvisi SETroubleshoot"

#: ../src/setroubleshoot/browser.py:260 ../gui/browser.glade.h:37
msgid "List All Alerts"
msgstr "Elenca tutti gli allarmi"

#: ../src/setroubleshoot/browser.py:307
msgid "#"
msgstr "#"

#: ../src/setroubleshoot/browser.py:307
msgid "Source Process"
msgstr "Fonte processo"

#: ../src/setroubleshoot/browser.py:307
msgid "Attempted Access"
msgstr "Accesso tentato"

#: ../src/setroubleshoot/browser.py:307
msgid "On this"
msgstr "Su questo"

#: ../src/setroubleshoot/browser.py:307
msgid "Occurred"
msgstr "Si è verificato"

#: ../src/setroubleshoot/browser.py:307
msgid "Status"
msgstr "Stato"

#: ../src/setroubleshoot/browser.py:382 ../src/setroubleshoot/browser.py:818
#: ../src/setroubleshoot/browser.py:862
msgid "Notify"
msgstr "Notifica"

#: ../src/setroubleshoot/browser.py:383
msgid "Notify alert in the future."
msgstr "Notifica avviso in futuro."

#: ../src/setroubleshoot/browser.py:386 ../src/setroubleshoot/browser.py:820
#: ../src/setroubleshoot/browser.py:860 ../gui/browser.glade.h:29
msgid "Ignore"
msgstr "Ignora"

#: ../src/setroubleshoot/browser.py:387 ../gui/browser.glade.h:30
msgid "Ignore alert in the future."
msgstr "Ignora allarme in futuro."

#: ../src/setroubleshoot/browser.py:424
msgid "<b>If you were trying to...</b>"
msgstr "<b>Se si stava cercando di...</b>"

#: ../src/setroubleshoot/browser.py:433
msgid "<b>Then this is the solution.</b>"
msgstr "<b>Allora la soluzione è questa.</b>"

#: ../src/setroubleshoot/browser.py:531
msgid "Plugin\n"
"Details"
msgstr "Plugin\n"
"Dettagli"

#: ../src/setroubleshoot/browser.py:546
msgid "Report\n"
"Bug"
msgstr "Segnala\n"
"Bug"

#: ../src/setroubleshoot/browser.py:576
#, python-format
msgid "Plugin: %s "
msgstr "Plugin: %s "

#: ../src/setroubleshoot/browser.py:643
msgid "Unable to grant access."
msgstr "Impossibile conferire l'accesso."

#: ../src/setroubleshoot/browser.py:674 ../src/setroubleshoot/browser.py:816
#, python-format
msgid "Alert %d of %d"
msgstr "Allarme %d di %d"

#: ../src/setroubleshoot/browser.py:799
#, python-format
msgid "On this %s:"
msgstr "Su questo %s:"

#: ../src/setroubleshoot/browser.py:847 ../src/setroubleshoot/browser.py:853
#: ../src/setroubleshoot/signature.py:434
msgid "N/A"
msgstr "N/A"

#: ../src/setroubleshoot/browser.py:894 ../src/setroubleshoot/browser.py:895
#: ../gui/browser.glade.h:34
msgid "No Alerts"
msgstr "Niente allarmi"

#: ../src/setroubleshoot/browser.py:913
msgid "SELinux has detected a problem."
msgstr "SELinux ha rilevato un problema."

#: ../src/setroubleshoot/browser.py:1032
msgid "Sealert Error"
msgstr "Errore Sealert"

#: ../src/setroubleshoot/browser.py:1045
msgid "Sealert Message"
msgstr "Messaggio Sealert"

#. -----------------------------------------------------------------------------
#: ../src/setroubleshoot/errcode.py:83
msgid "signature not found"
msgstr "firma non trovata"

#: ../src/setroubleshoot/errcode.py:84
msgid "multiple signatures matched"
msgstr "firme multiple corrispondenti"

#: ../src/setroubleshoot/errcode.py:85
msgid "id not found"
msgstr "id non trovato"

#: ../src/setroubleshoot/errcode.py:86
msgid "database not found"
msgstr "database non trovato"

#: ../src/setroubleshoot/errcode.py:87
msgid "item is not a member"
msgstr "la voce non è un membro"

#: ../src/setroubleshoot/errcode.py:88
msgid "illegal to change user"
msgstr "il cambio utente non è consentito"

#: ../src/setroubleshoot/errcode.py:89
msgid "method not found"
msgstr "metodo non trovato"

#: ../src/setroubleshoot/errcode.py:90
msgid "cannot create GUI"
msgstr "impossibile creare GUI"

#: ../src/setroubleshoot/errcode.py:91
msgid "value unknown"
msgstr "valore sconosciuto"

#: ../src/setroubleshoot/errcode.py:92
msgid "cannot open file"
msgstr "impossibile aprire il file"

#: ../src/setroubleshoot/errcode.py:93
msgid "invalid email address"
msgstr "indirizzo email non valido"

#. gobject IO Errors
#: ../src/setroubleshoot/errcode.py:96
msgid "socket error"
msgstr "errore socket"

#: ../src/setroubleshoot/errcode.py:97
msgid "connection has been broken"
msgstr "la connessione è stata interrotta"

#: ../src/setroubleshoot/errcode.py:98
msgid "Invalid request. The file descriptor is not open"
msgstr "Richiesta non valida. Il descrittore del file non è aperto"

#: ../src/setroubleshoot/errcode.py:100
msgid "insufficient permission to modify user"
msgstr "permessi insufficienti per modificare l'utente"

#: ../src/setroubleshoot/errcode.py:101
msgid "authentication failed"
msgstr "autenticazione fallita"

#: ../src/setroubleshoot/errcode.py:102
msgid "user prohibited"
msgstr "utente proibito"

#: ../src/setroubleshoot/errcode.py:103
msgid "not authenticated"
msgstr "non autenticato"

#: ../src/setroubleshoot/errcode.py:104
msgid "user lookup failed"
msgstr "lookup utente fallito"

#: ../src/setroubleshoot/gui_utils.py:56 ../src/sealert:565
#, c-format, python-format
msgid "Opps, %s hit an error!"
msgstr "Opps, %s ha incontrato un errore!"

#: ../src/setroubleshoot/gui_utils.py:58 ../gui/fail_dialog.glade.h:1
msgid "Error"
msgstr "Errore"

#: ../src/setroubleshoot/Plugin.py:92
msgid ""
"If you want to allow $SOURCE_BASE_PATH to have $ACCESS access on the "
"$TARGET_BASE_PATH $TARGET_CLASS"
msgstr ""
"Se vuoi abilitare $SOURCE_BASE_PATH ad avere $ACCESS accesso al "
"$TARGET_BASE_PATH $TARGET_CLASS"

#: ../src/setroubleshoot/server.py:226
#, python-format
msgid " For complete SELinux messages run: sealert -l %s"
msgstr " Per i messaggi SELinux completi, eseguire: sealert -l %s"

#: ../src/setroubleshoot/server.py:414
#, python-format
msgid "The user (%s) cannot modify data for (%s)"
msgstr "L'utente (%s) non può modificare i dati per (%s)"

#: ../src/setroubleshoot/server.py:486 ../src/sealert:318
msgid "Started"
msgstr "Avviato"

#: ../src/setroubleshoot/server.py:684
msgid "AVC"
msgstr "AVC"

#: ../src/setroubleshoot/server.py:775
msgid "Audit Listener"
msgstr "Ascoltatore audit"

#: ../src/setroubleshoot/signature.py:89
msgid "Never Ignore"
msgstr "Non ignorare mai"

#: ../src/setroubleshoot/signature.py:90
msgid "Ignore Always"
msgstr "Ignora sempre"

#: ../src/setroubleshoot/signature.py:91
msgid "Ignore After First Alert"
msgstr "Ignora dopo il primo avviso"

#: ../src/setroubleshoot/signature.py:213
msgid "directory"
msgstr "cartella"

#: ../src/setroubleshoot/signature.py:214
msgid "semaphore"
msgstr "semaforo"

#: ../src/setroubleshoot/signature.py:215
msgid "shared memory"
msgstr "memoria condivisa"

#: ../src/setroubleshoot/signature.py:216
msgid "message queue"
msgstr "coda dei messaggi"

#: ../src/setroubleshoot/signature.py:217
msgid "message"
msgstr "messaggio"

#: ../src/setroubleshoot/signature.py:218
msgid "file"
msgstr "file"

#: ../src/setroubleshoot/signature.py:219
msgid "socket"
msgstr "socket"

#: ../src/setroubleshoot/signature.py:220
msgid "process"
msgstr "processo"

#: ../src/setroubleshoot/signature.py:221
msgid "process2"
msgstr "process2"

#: ../src/setroubleshoot/signature.py:222
msgid "filesystem"
msgstr "filesystem"

#: ../src/setroubleshoot/signature.py:223
msgid "node"
msgstr "nodo"

#: ../src/setroubleshoot/signature.py:224
msgid "capability"
msgstr "capacità"

#: ../src/setroubleshoot/signature.py:225
msgid "capability2"
msgstr "capability2"

#: ../src/setroubleshoot/signature.py:418
#, python-format
msgid "%s has a permissive type (%s). This access was not denied."
msgstr ""
"%s presenta una tipologia permissiva (%s). Questo accesso non è stato negato."
""

#: ../src/setroubleshoot/signature.py:421
msgid "SELinux is in permissive mode. This access was not denied."
msgstr "SELinux è in modalità permissiva. L'accesso non è stato negato."

#: ../src/setroubleshoot/signature.py:475
#, python-format
msgid "SELinux is preventing %s from using the %s access on a process."
msgstr "SELinux impedisce a %s un accesso %s su un processo."

#: ../src/setroubleshoot/signature.py:475
#, python-format
msgid "SELinux is preventing %s from using the '%s' accesses on a process."
msgstr "SELinux impedisce a %s accessi '%s' su un processo."

#: ../src/setroubleshoot/signature.py:478
#, python-format
msgid "SELinux is preventing %s from using the %s capability."
msgstr "SELinux impedisce a %s di utilizzare la capacità %s."

#: ../src/setroubleshoot/signature.py:478
#, python-format
msgid "SELinux is preventing %s from using the '%s' capabilities."
msgstr "SELinux impedisce a %s di utilizzare le capacità '%s'."

#: ../src/setroubleshoot/signature.py:480
#, python-format
msgid "SELinux is preventing %s from %s access on the %s labeled %s."
msgstr "SELinux sta impedendo a %s dall'accesso %s sul %s con etichetta %s."

#: ../src/setroubleshoot/signature.py:480
#, python-format
msgid "SELinux is preventing %s from '%s' accesses on the %s labeled %s."
msgstr "SELinux sta impedendo %s dagli accessi '%s' sul %s con etichetta %s."

#: ../src/setroubleshoot/signature.py:481
#, python-format
msgid "SELinux is preventing %s from %s access on the %s %s."
msgstr "SELinux impedisce a %s un accesso %s su %s %s."

#: ../src/setroubleshoot/signature.py:481
#, python-format
msgid "SELinux is preventing %s from '%s' accesses on the %s %s."
msgstr "SELinux impedisce a %s accessi '%s' su %s %s."

#: ../src/setroubleshoot/signature.py:526
msgid "Additional Information:\n"
msgstr "Informazioni addizionali:\n"

#: ../src/setroubleshoot/signature.py:527
msgid "Source Context"
msgstr "Contesto della sorgente"

#: ../src/setroubleshoot/signature.py:528
msgid "Target Context"
msgstr "Contesto target"

#: ../src/setroubleshoot/signature.py:529
msgid "Target Objects"
msgstr "Oggetti target"

#: ../src/setroubleshoot/signature.py:530
msgid "Source"
msgstr "Sorgente"

#: ../src/setroubleshoot/signature.py:531
msgid "Source Path"
msgstr "Percorso della sorgente"

#: ../src/setroubleshoot/signature.py:532
msgid "Port"
msgstr "Porta"

#: ../src/setroubleshoot/signature.py:534
#: ../src/setroubleshoot/signature.py:536
msgid "Host"
msgstr "Host"

#: ../src/setroubleshoot/signature.py:537
msgid "Source RPM Packages"
msgstr "Sorgente Pacchetti RPM"

#: ../src/setroubleshoot/signature.py:538
msgid "Target RPM Packages"
msgstr "Pacchetti RPM target"

#: ../src/setroubleshoot/signature.py:539
msgid "SELinux Policy RPM"
msgstr ""

#: ../src/setroubleshoot/signature.py:540
msgid "Local Policy RPM"
msgstr ""

#: ../src/setroubleshoot/signature.py:541
msgid "Selinux Enabled"
msgstr "Selinux abilitato"

#: ../src/setroubleshoot/signature.py:542
msgid "Policy Type"
msgstr "Tipo di politica"

#: ../src/setroubleshoot/signature.py:543
msgid "Enforcing Mode"
msgstr "Modalità Enforcing"

#: ../src/setroubleshoot/signature.py:545
#: ../src/setroubleshoot/signature.py:547
msgid "Host Name"
msgstr "Host Name"

#: ../src/setroubleshoot/signature.py:552
#: ../src/setroubleshoot/signature.py:554
msgid "Platform"
msgstr "Piattaforma"

#: ../src/setroubleshoot/signature.py:555
msgid "Alert Count"
msgstr "Conteggio avvisi"

#: ../src/setroubleshoot/signature.py:557
msgid "First Seen"
msgstr "Primo visto"

#: ../src/setroubleshoot/signature.py:558
msgid "Last Seen"
msgstr "Ultimo visto"

#: ../src/setroubleshoot/signature.py:559
msgid "Local ID"
msgstr "ID locale"

#: ../src/setroubleshoot/signature.py:561
msgid "Raw Audit Messages"
msgstr "Messaggi Raw Audit"

#: ../src/setroubleshoot/signature.py:615
#, python-format
msgid "\n"
"\n"
"*****  Plugin %s (%.4s confidence) suggests   "
msgstr "\n"
"⏎\n"
"⏎\n"
"***** Plugin %s(%.4s confidenza) suggerisce"

#: ../src/setroubleshoot/signature.py:618
msgid "*"
msgstr "*"

#: ../src/setroubleshoot/signature.py:619
#: ../src/setroubleshoot/signature.py:621
msgid "\n"
msgstr "\n"

#: ../src/setroubleshoot/signature.py:623
msgid "\n"
"Then "
msgstr "\n"
"Quindi "

#: ../src/setroubleshoot/signature.py:626
msgid "\n"
"Do\n"
msgstr "\n"
"Fai\n"

#: ../src/setroubleshoot/signature.py:628
msgid "\n"
"\n"
msgstr "\n"
"\n"

#: ../src/seappletlegacy.c:189 ../src/seapplet:133
msgid "New SELinux security alert"
msgstr "Nuovo avviso di sicurezza SELinux"

#: ../src/seappletlegacy.c:189 ../src/seapplet:134
msgid "AVC denial, click icon to view"
msgstr "Rifiuto AVC, fare click sull'icona per vedere"

#: ../src/seappletlegacy.c:195 ../src/seappletlegacy.c:246 ../src/seapplet:137
msgid "Dismiss"
msgstr "Ignora"

#: ../src/seappletlegacy.c:205 ../src/seappletlegacy.c:256 ../src/seapplet:138
#: ../src/seapplet:139
msgid "Show"
msgstr "Mostra"

#. set tooltip
#: ../src/seappletlegacy.c:440 ../src/sealert:71
msgid "SELinux AVC denial, click to view"
msgstr "Rifiuto AVC di SELinux, fare click per vedere"

#: ../src/seapplet:160
msgid "SELinux Troubleshooter: Applet requires SELinux be enabled to run"
msgstr ""
"Risoluzione dei problemi SELinux: l'applet richiede che SELinux sia "
"abilitato per l'esecuzione"

#: ../src/sealert:121
msgid "SELinux not enabled, sealert will not run on non SELinux systems"
msgstr ""
"SELinux non è abilitato, sealert non funzionerà su sistemi non SELinux"

#: ../src/sealert:171
msgid "Not fixable."
msgstr "Non riparabile."

#: ../src/sealert:178
#, c-format
msgid "Successfully ran %s"
msgstr "%s eseguito con successo"

#: ../src/sealert:185
#, c-format
msgid "Plugin %s not valid for %s id"
msgstr "Il plugin %s non è valido per l'id %s"

#: ../src/setroubleshootd:79
msgid "SELinux not enabled, setroubleshootd exiting..."
msgstr "SELinux non abilitato, setroubleshootd sta per uscire..."

#: ../src/setroubleshootd:111
#, c-format
msgid "fork #1 failed: %d (%s)"
msgstr "fork #1 fallito: %d (%s)"

#: ../gui/browser.glade.h:1
msgid ""
"Copyright (c) 2010\n"
"Thomas Liu <tliu@redhat.com>\n"
"Máirín Duffy <duffy@redhat.com>\n"
"Daniel Walsh <dwalsh@redhat.com>\n"
"John Dennis <jdennis@redhat.com>\n"
msgstr ""
"Copyright (c) 2010 Thomas Liu <tliu@redhat.com>Máirín Duffy <duffy@redhat."
"com>Daniel Walsh <dwalsh@redhat.com>John Dennis <jdennis@redhat.com>\n"

#: ../gui/browser.glade.h:9
msgid "Troubleshoot selected alert"
msgstr "Risoluzione dei problemi dell'allarme selezionato"

#: ../gui/browser.glade.h:10
msgid "Delete"
msgstr "Elimina"

#: ../gui/browser.glade.h:11
msgid "Delete Selected Alerts"
msgstr "Cancellare gli allarmi selezionati"

#: ../gui/browser.glade.h:13
msgid "Close"
msgstr "Chiudi"

#: ../gui/browser.glade.h:15
msgid "<b>SELinux has detected a problem.</b>"
msgstr "<b>SELinux ha trovato un problema.</b>"

#: ../gui/browser.glade.h:18
msgid "Turn on alert pop-ups."
msgstr "Attiva i popup d'allarme."

#: ../gui/browser.glade.h:20
msgid "Turn off alert pop-ups."
msgstr "Disattiva i popup d'allarme."

#: ../gui/browser.glade.h:23
msgid "On this file:"
msgstr "Su questo file:"

#: ../gui/browser.glade.h:24
msgid "label"
msgstr "etichetta"

#: ../gui/browser.glade.h:25
msgid ""
"Read alert troubleshoot information.  May require administrative privileges "
"to remedy."
msgstr ""
"Leggere informazioni sulla risoluzione dei problemi. Si potrebbero "
"richiedere privilegi di amministratore per rimediare."

#: ../gui/browser.glade.h:27
msgid "Email alert to system administrator."
msgstr "Invia via email l'allarme all'amministratore del sistema."

#: ../gui/browser.glade.h:31
msgid "Delete current alert from the database."
msgstr "Cancellare gli allarmi correnti dal database."

#: ../gui/browser.glade.h:32
msgid "Previous"
msgstr "Precedente"

#: ../gui/browser.glade.h:33
msgid "Show previous alert."
msgstr "Mostra allarme precedente."

#: ../gui/browser.glade.h:35
msgid "Next"
msgstr "Avanti"

#: ../gui/browser.glade.h:36
msgid "Show next alert."
msgstr "Mostra prossimo allarme."

#: ../gui/browser.glade.h:38
msgid "List all alerts in the database."
msgstr "Elenca tutti gli allarmi nel database."

#: ../gui/bug_report.glade.h:1
msgid "Review and Submit Bug Report"
msgstr "Rivedi e sottometti il rapporto bug "

#: ../gui/bug_report.glade.h:2
msgid "<span size='large' weight='bold'>Review and Submit Bug Report</span>"
msgstr ""
"<span size='large' weight='bold'>Rivedi e Sottometti il Bug Report</span>"

#: ../gui/bug_report.glade.h:3
msgid ""
"You may wish to review the error output that will be included in this bug "
"report and modify it to exclude any sensitive data below."
msgstr ""
"È possibile controllare l'output dell'errore che sarà incluso in questo bug "
"report e modificarlo per escludere qualsiasi dato sensibile."

#: ../gui/bug_report.glade.h:4
msgid "Included error output:"
msgstr "Output errore incluso:"

#: ../gui/bug_report.glade.h:5
msgid "Submit Report"
msgstr "Sottometti rapporto"

#: ../gui/fail_dialog.glade.h:2 ../gui/success_dialog.glade.h:2
msgid ""
"This operation was completed.  The quick brown fox jumped over the lazy dog."
msgstr ""
"Questa operazione è stata completata. Ma la volpe col suo balzo ha raggiunto "
"il quieto fido."

#: ../gui/success_dialog.glade.h:1
msgid "Success!"
msgstr "Successo!"

#: ../gui/success_dialog.glade.h:3
msgid "button"
msgstr "tasto"
